import api from "../config/api";

const vendorService = {
    getList(params) {
        return api.get('/mobile/v3/restaurant/vendors-list', {params})
            .then(({data}) => data);
    }
}

export default vendorService;
