import React, {useEffect} from 'react';
import Head from "next/head";
import InfiniteScroll from "react-infinite-scroll-component";
import VendorCard from "../components/vendorCard/VendorCard";
import {useDispatch, useSelector} from "react-redux";
import {fetchVendors} from "../store/actions/vendorActions";

const Restaurant = () => {

    const {data: vendors, page,hasMore} = useSelector(state => state.vendor);
    const dispatch = useDispatch();

    const onGetVendors = () => {
        dispatch(fetchVendors({page}));
    }

    useEffect(() => {
        onGetVendors();
    }, []);

    return (
        <div>
            <Head>
                <title>سفارش آنلاین از رستوران‌ها، ‌شیرینی‌فروشی‌ها، کافی‌شاپ‌ها، سوپرمارکت‌ها،‌ نانوایی‌ها و ...</title>
                <meta name="description" content="اسنپ فود (زودفود قدیم) سامانه سفارش آنلاین غذا، شیرینی و خرید آنلاین از کافی شاپ و سوپرمارکت ها در تهران، کرج، شیراز، اصفهان، مشهد و سراسر ایران"/>
            </Head>
            <InfiniteScroll
                dataLength={vendors.length}
                next={onGetVendors}
                scrollThreshold={'100px'}
                hasMore={hasMore}
                loader={<p>در حال دریافت اطلاعات</p>}
                endMessage={
                    <p>
                        <b>done</b>
                    </p>
                }
            >
                {vendors.map(vendor => (
                    <VendorCard key={vendor.id} vendor={vendor}/>
                ))}
            </InfiniteScroll>

        </div>
    );
};


export default Restaurant;