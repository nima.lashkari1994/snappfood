import Head from 'next/head'
import Link from 'next/link'

const Home = () => {

    return (
        <div>
            <Head>
                <title>سفارش آنلاین از رستوران‌ها، ‌شیرینی‌فروشی‌ها، کافی‌شاپ‌ها، سوپرمارکت‌ها،‌ نانوایی‌ها و ...</title>
                <meta name="description" content="اسنپ فود (زودفود قدیم) سامانه سفارش آنلاین غذا، شیرینی و خرید آنلاین از کافی شاپ و سوپرمارکت ها در تهران، کرج، شیراز، اصفهان، مشهد و سراسر ایران"/>
            </Head>

            <main>
                <h1>
                    اسنپ فود
                </h1>
                <Link href="/restaurant">
                    <a>رستوران‌ها</a>
                </Link>
            </main>

        </div>
    )
}

export default Home
