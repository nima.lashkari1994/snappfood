import '../styles/globals.scss'
import '../styles/variables.scss';
import {Provider} from "react-redux";
import store from "../store";
import Layout from "../components/layout/Layout";

function MyApp({Component, pageProps}) {
    return (
        <Provider store={store}>
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </Provider>
    )
}

export default MyApp;
