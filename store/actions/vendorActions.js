import vendorService from "../../services/vendorService";

export const VENDOR_FETCH_SUCCESS = 'VENDOR_FETCH_SUCCESS';

export const fetchVendors = ({page=0}) => {
    return (dispatch) => {
        //here we can dispatch an action for handling custom loading
        vendorService.getList({page,page_size:10,lat:35.774, long:51.418})
            .then(({data}) => {
                dispatch(fetchVendorsSuccess(data))
            })
            .catch(err => {
                //here we can dispatch an action for handling errors
                throw err;
            })
    }
};

export const fetchVendorsSuccess = vendors => {
    return {
        type: VENDOR_FETCH_SUCCESS,
        payload: vendors
    }
}
