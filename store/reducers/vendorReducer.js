import {VENDOR_FETCH_SUCCESS} from "../actions/vendorActions";

const initialState = {
    data:[],
    page:0,
    hasMore:true,
    //error:'',
    //loading:false
}
const vendorReducer = (state=initialState,action) => {

    switch (action.type){
        case VENDOR_FETCH_SUCCESS: {
            const vendors = action.payload.finalResult.slice(-9).map(({data}) => data)
            return {
                data: [...state.data, ...vendors],
                page: state.page + 1,
                hasMore: vendors.length
            }
        }
        default:
            return state;
    }
}

export default vendorReducer;