import vendorReducer from "./vendorReducer";
import {combineReducers} from "redux";


const rootReducer = combineReducers({
    vendor:vendorReducer
});


export default rootReducer;