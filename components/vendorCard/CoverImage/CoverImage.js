import React from 'react';
import Image from "next/image";

const CoverImage = ({src, width, height, ...props}) => {

    const loader = ({src, width}) => {
        return src.replace('w.x.h', `${width}x${width*0.5}`);
    }

    return (
        <Image
            sizes="(min-width: 800px) 33vw,
              50vw"
            src={src} loader={loader} width={width} height={height} {...props}/>
    );
};

export default CoverImage;