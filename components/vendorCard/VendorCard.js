import React from 'react';
import Image from "next/image";
import styles from "./VendorCard.module.scss";
import CoverImage from "./CoverImage/CoverImage";
import StarIcon from "../icons/StarIcon";

const VendorCard = ({vendor}) => {
    const {title, logo, backgroundImageCustom, rate, isZFExpress, deliveryFee, description, countReview} = vendor;
    return (
        <section className={styles.vendor}>
            <header className={styles.vendor__header}>
                <CoverImage src={backgroundImageCustom} width={565} height={133} objectFit={"cover"}
                            layout={"responsive"}/>
                <div className={styles.vendor__logo}>
                    <Image src={logo} width={56} height={56}/>
                </div>
            </header>
            <div className={styles.vendor__details}>
                <div className={styles.vendor__details__row}>
                    <h3 className={styles.vendor__title}>{title}</h3>
                    <div className={styles.vendor__review}>
                        <span className={styles.vendor__review__count}>({countReview})</span>
                        <span className={styles.vendor__review__rate}>
                        {rate?.toFixed(1)}
                            <StarIcon className={styles.vendor__review__rate__icon}/>
                    </span>
                    </div>
                </div>
                <div className={styles.vendor__details__row}>
                    <div className={styles.vendor__description}>{description}</div>
                </div>
                <div className={styles.vendor__details__row}>
                    <div>
                        <span className={styles.vendor__delivery}>{isZFExpress ? 'ارسال‌اکسپرس' : 'پیک فروشنده'}</span>
                        <span className={styles.vendor__price}>{deliveryFee?.toLocaleString()} تومان</span>
                    </div>

                </div>
            </div>
        </section>
    );
};

export default VendorCard;