import axios from "axios";
import {SERVER_URL} from "./constants";

const api = axios.create({
    baseURL: SERVER_URL,
    headers: {'Content-Type': 'application/json'},
    responseType: 'json',
});

//just for illustration purposes
api.interceptors.request.use((config) => {
    return {
        ...config,
        headers: {
            // 'Authorization': storage.getToken(),
            ...config.headers,
        }
    };
});

export default api;