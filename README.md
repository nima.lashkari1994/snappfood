# Documentation

## folder structure

### components
global components are placed here . 

### config
project configs like axios , constants , project setup scripts , storage management 

### pages
special nextjs directory to create routes based on files in it.

### services
api calls are managed here to encapsulate api shapes like params, headers , ...

### store
all the redux stuff

